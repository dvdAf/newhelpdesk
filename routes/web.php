<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//prefixo rota adm
Route::group(['prefix' => 'adm'],function()
{
    Route::get('/', function ()
    {
        return "ADM HOME";
    });
});

Route::get('/', function ()
{
    return view('welcome');
});
